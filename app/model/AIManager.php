<?php
/**
 * Created by PhpStorm.
 * User: PanYame
 * Date: 14.1.2018
 * Time: 22:29
 */

namespace App\Model;


class AIManager
{
    public $field, $aiSymbol, $playerSymbol;

    public function __construct(array $field, int $aiSymbol)
    {
        $this->field = $field;
        $this->aiSymbol = $aiSymbol;
        $this->playerSymbol = $aiSymbol == 1 ? 2 : 1;
    }

    /**
     * @return array Play coordination
     */
    public function play(): array
    {
        $defend = $this->shouldDefend();
        $attack = $this->shouldAttack();
        if ($defend["points"] > $attack["points"]) {
            return $defend["coords"];
        } else {
            return $attack["coords"];
        }
    }

    /**
     * @return array --> [INT POINTS, ARRAY COORDS[COL, ROW]]
     */
    private function shouldDefend(): array
    {
        // start counting from higher points
        $points = 0;
        $coords = [rand(0, BaseRepository::GAMEFIELD_SIZE-1), rand(0, BaseRepository::GAMEFIELD_SIZE-1)];
        for($col = 0; $col <= BaseRepository::GAMEFIELD_SIZE-1; $col++) {
            for ($row = 0; $row <= BaseRepository::GAMEFIELD_SIZE - 1; $row++) {
                $current = $this->field[$col][$row];

                // three in a row both sides clear => 3 points
                if ($current == $this->playerSymbol &&
                    @$this->field[$col][$row - 1] == 0 &&
                    @$this->field[$col][$row + 1] == $this->playerSymbol &&
                    @$this->field[$col][$row + 2] == $this->playerSymbol &&
                    @$this->field[$col][$row + 3] == 0) {
                    $points = 3;
                    $coords = [$col, $row+3];
                }

            }
        }
        return [
            "points" => $points,
            "coords" =>
                [
                    "col" => $coords[0],
                    "row" => $coords[1]
                ]
        ];
    }

    /**
     * @return array Same as defend
     */
    public function shouldAttack(): array
    {
        $points = 0;
        $coords = [rand(0, BaseRepository::GAMEFIELD_SIZE-1), rand(0, BaseRepository::GAMEFIELD_SIZE-1)];

        return [
            "points" => $points,
            "coords" =>
                [
                    "col" => $coords[0],
                    "row" => $coords[1]
                ]
        ];
    }
}