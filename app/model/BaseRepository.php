<?php
/**
 * Created by PhpStorm.
 * User: PanYame
 * Date: 14.1.2018
 * Time: 0:56
 */

namespace App\Model;

use Nette;

class BaseRepository
{
    public $session;

    /** @var array $gameField */
    public $gameField;

    public $db;

    const GAMEFIELD_SIZE = 16;

    public function __construct(Nette\Http\Session $session, Nette\Database\Context $db)
    {
        $this->session = $session;
        $this->game = $session->getSection("game");
        $this->db = $db;
    }

    /**
     * Function in_array that works with multidimensional arrays
     * @param $needle
     * @param $haystack
     * @param bool $strict
     * @return bool
     */
    public function in_array_r($needle, $haystack, $strict = false) {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && $this->in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }
}