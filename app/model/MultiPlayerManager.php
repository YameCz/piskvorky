<?php
/**
 * Created by PhpStorm.
 * User: PanYame
 * Date: 15.1.2018
 * Time: 21:46
 */

namespace App\Model;

use Nette;

class MultiPlayerManager extends BaseRepository
{
    /**
     * @param int $playerSymbol
     * @param string $playerName
     * @param bool $firstPlay
     */
    public function createGame(int $playerSymbol, string $playerName, bool $firstPlay)
    {
        $hash = substr(md5(rand().rand()),0,3);
        $map = json_encode($this->initEmptyGameField());

        $this->db->table("games")->insert([
            "created_at" => time(),
            "hash" => $hash,
            "map" => $map
        ]);

        $game = $this->db->table("games")->where("hash ?", $hash)->fetch();
        $firstPlayDB = $firstPlay ? 1 : 0;

        $this->db->table("players")->insert([
            "name" => $playerName,
            "game_id" => $game->id,
            "symbol" => $playerSymbol,
            "first_play" => $firstPlayDB
        ]);

        $player = $this->db->table("players")->where("game_id ?", $game->id)->fetch();

        $this->game->hash = $hash;
        $this->game->gameId = $game->id;
        $this->game->playerId = $player->id;
    }

    /**
     * @param mixed $code
     * @return bool
     */
    public function validateCode($code): bool
    {
        $row = $this->db->table("games")->where("hash ?", $code);
        if ($row->fetch()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param string $code
     * @param string $playerName
     * @return bool
     */
    public function joinGame(string $code, string $playerName): bool
    {
        $game = $this->db->table("games")->where("hash ?", $code)->fetch();
        $player = $this->db->table("players")->where("game_id ?", $game->id);
        if ($player->count("*") > 1) {
            return false;
        } else {

            $player = $player->fetch();
            $playerSymbol = $player->symbol == 1 ? 2 : 1;
            $firstPlay = $player->first_play == 1 ? 0 : 1;

            $this->db->table("players")->insert([
                "name" => $playerName,
                "game_id" => $game->id,
                "symbol" => $playerSymbol,
                "first_play" => $firstPlay
            ]);

            $player = $this->db->table("players")->where("game_id ? AND symbol ?", $game->id, $playerSymbol)->fetch();

            $this->game->hash = $code;
            $this->game->gameId = $game->id;
            $this->game->playerId = $player->id;

            return true;
        }
    }

    /**
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getGame()
    {
        $row = $this->db->table("games")->where("id ?", $this->game->gameId)->fetch();
        return $row;
    }

    /**
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getPlayer()
    {
        $row = $this->db->table("players")->where("id ?", $this->game->playerId)->fetch();
        return $row;
    }

    /**
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getPlayer2()
    {
        $row = $this->db->table("players")->where("game_id ? AND NOT id ?", $this->game->gameId, $this->game->playerId)->fetch();
        return $row;
    }

    public function logout()
    {
        //delete game from database
        $this->db->table("games")->where("id ?", $this->game->gameId)->delete();
        $this->db->table("players")->where("game_id ?", $this->game->gameId)->delete();

        $this->game->remove();
    }

    /**
     * @return array Matrix fulled with zeros
     */
    public function initEmptyGameField(): array
    {
        $field = [];
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                $field[$col][$row] = 0;
            }
        }
        return $field;
    }

    /**
     * Handle move
     * @param int col number
     * @param int row number
     * @param int playerId
     */
    public function play(int $col, int $row, int $playerId)
    {
        $map = json_decode($this->getMap());
        $map[$col][$row] = $this->getPlayer()->symbol;
        $lastMove = json_encode([$col,$row]);
        $map = json_encode($map);
        $this->db->table("games")->where("id ?", $this->game->gameId)->update([
            "map" => $map,
            "last_move" => $lastMove
        ]);
    }

    /**
     * @param int $col
     * @param int $row
     * @return bool
     */
    public function canMove(int $col, int $row)
    {
        //todo: check if player turn (prevent playing more than one)
        $map = json_decode($this->getMap());
        $playerMovesCount = $this->countMoves($this->getPlayer()->symbol, $map);
        $player2MovesCount = $this->countMoves($this->getPlayer2()->symbol, $map);
        if ($playerMovesCount < $player2MovesCount) {
            return $map[$col][$row] == 0;
        } else if ($playerMovesCount == $player2MovesCount && $this->getPlayer()->first_play == 1) {
            return $map[$col][$row] == 0;
        } else {
            return false;
        }
    }

    /**
     * @return object Player reference
     */
    public function whoIsPlaying()
    {
        // don't rename player1 to player
        $map = json_decode($this->getMap());
        $player1MovesCount = $this->countMoves($this->getPlayer()->symbol, $map);
        $player2MovesCount = $this->countMoves($this->getPlayer2()->symbol, $map);
        if ($player1MovesCount < $player2MovesCount) {
            return $this->getPlayer();
        } else if ($player1MovesCount == $player2MovesCount && $this->getPlayer()->first_play == 1) {
            return $this->getPlayer();
        } else {
            return $this->getPlayer2();
        }
    }

    /**
     * @return bool
     */
    public function isSecondPlayerConnected(): bool
    {
        $row = $this->db->table("players")->where("game_id ?", $this->game->gameId);
        if ($row->count("*") < 2) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param int $playerId
     */
    public function addPointToScore(int $playerId)
    {
        // check if player score is valid
        $sumScore = $this->getPlayer()->score + $this->getPlayer2()->score;
        $row = $this->db->table("games")->select("round")->where("id ?", $this->game->gameId)->fetch();
        if ($sumScore == $row->round) {
            $row = $this->db->table("players")->select("score")->where("id ?", $playerId);
            $score = $row->fetch()->score + 1;
            $row->update([
                "score" => $score
            ]);
        }
    }

    public function addPointsToScoreDraw()
    {
        // check if player score is valid
        $sumScore = $this->getPlayer()->score + $this->getPlayer2()->score;
        $gameRow = $this->db->table("games")->select("round")->where("id ?", $this->game->gameId);
        $round = $gameRow->fetch()->round;
        if ($sumScore == $round) {

            $row = $this->db->table("players")->select("score")->where("id ?", $this->game->playerId);
            $score = $row->fetch()->score + 1;
            $row->update([
                "score" => $score
            ]);

            $row = $this->db->table("players")->select("score")->where("id ?", $this->getPlayer2()->id);
            $score = $row->fetch()->score + 1;
            $row->update([
                "score" => $score
            ]);

            $round += 1;

            $gameRow->update([
                "round" => $round
            ]);
        }
    }

    /**
     * @param int $playerSymbol
     * @param array $map
     * @return int Moves count
     */
    private function countMoves(int $playerSymbol, array $map): int
    {
        $count = 0;
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                if ($map[$col][$row] == $playerSymbol) {
                    $count++;
                }
            }
        }
        return $count;
    }

    /**
     * @param int $symbol
     * @return bool|mixed|\Nette\Database\Table\IRow
     */
    public function getPlayerBySymbol(int $symbol)
    {
        $row = $this->db->table("players")->where("game_id ? AND symbol ?", $this->game->gameId, $symbol)->fetch();
        return $row;
    }

    /**
     * @return bool|Nette\Database\Table\Selection
     */
    public function getMap()
    {
        $row = $this->db->table("games")->select("map")->where("id ?", $this->game->gameId)->fetch();
        if ($row) {
            return $row->map;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function checkLogin(): bool
    {
        return isset($this->game->gameId);
    }

    /**
     * @return bool
     */
    public function isMapOk(): bool
    {
        if ($this->getMap() === false) {
            return false;
        } else {
            return true;
        }
    }

    public function newGame()
    {
        $round = $this->getGame()->round + 1;

        // initalize clear gamefield
        $this->db->table("games")->where("id ?", $this->game->gameId)->update([
            "map" => json_encode($this->initEmptyGameField()),
            "round" => $round,
            "last_move" => ""
        ]);

        // switch sides
        $player1NewSymbol = $this->getPlayer()->symbol == 1 ? 2 : 1;
        $player2NewSymbol = $this->getPlayer2()->symbol == 1 ? 2 : 1;
        $player1FirstPlay = $this->getPlayer()->first_play == 1 ? 0 : 1;
        $player2FirstPlay = $this->getPlayer2()->first_play  == 1 ? 0 : 1;

        $this->db->table("players")->where("id ?", $this->getPlayer()->id)->update([
            "symbol" => $player1NewSymbol,
            "first_play" => $player1FirstPlay,
            "next_game" => 0,
            "want_draw" => 0,
            "give_up" => 0
        ]);

        $this->db->table("players")->where("id ?", $this->getPlayer2()->id)->update([
            "symbol" => $player2NewSymbol,
            "first_play" => $player2FirstPlay,
            "next_game" => 0,
            "want_draw" => 0,
            "give_up" => 0
        ]);

    }

    /**
     * @param int $playerId
     */
    public function wantNextGame(int $playerId)
    {
        $this->db->table("players")->where("id ?", $playerId)->update([
            "next_game" => 1
        ]);
    }

    /**
     * @return bool
     */
    public function checkBothNewGame(): bool
    {
        return $this->getPlayer()->next_game == 1 && $this->getPlayer2()->next_game == 1;
    }

    /**
     * @return bool
     */
    public function checkDraw(): bool
    {
        return !$this->in_array_r(0, json_decode($this->getMap()));
    }

    /**
     * @return bool
     */
    public function checkDrawOffer()
    {
        $row = $this->db->table("players")->where("game_id ? AND want_draw ?", $this->game->gameId, 1)->fetch();
        if ($row) {
            if ($row->id != $this->game->playerId) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function declineDrawOffer()
    {
        $this->db->table("players")->where("want_draw = 1 AND game_id ?", $this->game->gameId)->update([
            "want_draw" => 2
        ]);
    }

    /**
     * @param int $playerId
     */
    public function offerDraw(int $playerId)
    {
        $this->db->table("players")->where("id ?", $playerId)->update([
            "want_draw" => 1
        ]);
    }

    public function castDraw()
    {
        $field = json_decode($this->getMap());
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                if ($field[$col][$row] == 0) {
                    $field[$col][$row] = PHP_INT_MAX;
                }
            }
        }
        $field = json_encode($field);
        $this->db->table("games")->where("id ?", $this->game->gameId)->update([
            "map" => $field
        ]);
    }

    public function giveUp()
    {
        $this->db->table("players")->where("id ?", $this->game->playerId)->update([
            "give_up" => 1
        ]);

        $this->addPointToScore($this->getPlayer2()->id);
    }

    public function checkGiveUp()
    {
        $row = $this->db->table("players")->where("game_id ? AND give_up = 1", $this->game->gameId)->fetch();
        if ($row) {
            return $row->id;
        } else {
            return false;
        }
    }

    public function canOfferDraw(): bool
    {
        return $this->getPlayer()->want_draw != 2;
    }

    /**
     * @return bool|array Player ID
     */
    public function gameFindWin()
    {
        $field = json_decode($this->getMap());
        $legalSymbols = [$this->getPlayer()->symbol, $this->getPlayer2()->symbol];
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                $current = $field[$col][$row];
                if (!in_array($current, $legalSymbols)) {
                    continue;
                }
                if ($current != 0) {

                    /*
                     * win: O O O O O
                     */

                    if ($current == @$field[$col][$row+1] AND
                        $current == @$field[$col][$row+2] AND
                        $current == @$field[$col][$row+3] AND
                        $current == @$field[$col][$row+4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col, $row+1],
                            [$col, $row+2],
                            [$col, $row+3],
                            [$col, $row+4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win: O
                     *      O
                     *      O
                     *      O
                     *      O
                     */

                    elseif ($current == @$field[$col-1][$row] AND
                        $current == @$field[$col-2][$row] AND
                        $current == @$field[$col-3][$row] AND
                        $current == @$field[$col-4][$row]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row],
                            [$col-2, $row],
                            [$col-3, $row],
                            [$col-4, $row],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win: O
                     *       O
                     *        O
                     *         O
                     *          O
                     */

                    elseif ($current == @$field[$col-1][$row+1] AND
                        $current == @$field[$col-2][$row+2] AND
                        $current == @$field[$col-3][$row+3] AND
                        $current == @$field[$col-4][$row+4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row+1],
                            [$col-2, $row+2],
                            [$col-3, $row+3],
                            [$col-4, $row+4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win:     O
                     *         O
                     *        O
                     *       O
                     *      O
                     */

                    elseif ($current == @$field[$col-1][$row-1] AND
                        $current == @$field[$col-2][$row-2] AND
                        $current == @$field[$col-3][$row-3] AND
                        $current == @$field[$col-4][$row-4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row-1],
                            [$col-2, $row-2],
                            [$col-3, $row-3],
                            [$col-4, $row-4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                }
            }
        }

        if (isset($winner)) {
            $this->addPointToScore($this->getPlayerBySymbol($winner)->id);
        }

        return isset($winner) && isset($winningRow) ? ["player" => $this->getPlayerBySymbol($winner), "row" => $winningRow] : false;
    }
}