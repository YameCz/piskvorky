<?php
/**
 * Created by PhpStorm.
 * User: PanYame
 * Date: 15.1.2018
 * Time: 21:45
 */

namespace App\Model;


class SinglePlayerManager extends BaseRepository
{
    
    /**
     * Handle move
     * @param int col number
     * @param int row number
     * @param int player
     * @return mixed win|not win
     */
    public function play(int $col, int $row, int $playerSymbol): bool
    {
        // check if field is empty first
        if ($this->game->field[$col][$row] == 0) {
            $this->game->field[$col][$row] = $playerSymbol;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param int $col
     * @param int $row
     * @param int $playerSymbol
     * @return bool
     */
    public function canPlay(int $col, int $row, int $playerSymbol): bool
    {
        $moves = $this->countMoves($playerSymbol, $this->game->field);
        $movesAI = $this->countMoves($this->game->playerAISymbol, $this->game->field);
        if ($moves < $movesAI) {
            return $this->game->gamefield[$col][$row] == 0;
        } else if ($moves == $movesAI && $this->game->playerStart) {
            return $this->game->gamefield[$col][$row] == 0;
        } else {
            return false;
        }
    }
    
    public function playAI()
    {
        $ai = new AIManager($this->game->field, $this->game->playerAISymbol);

        $this->play($ai->play()["col"], $ai->play()["row"], $this->game->playerAISymbol);
    }


    public function newGame()
    {
        // switch sides
        $tmp = $this->game->playerSymbol;
        $this->game->playerSymbol = $this->game->playerAISymbol;
        $this->game->playerAISymbol = $tmp;
        $this->game->playerStart = $this->game->playerStart ? false : true;
        unset($this->game->surrendered);

        // render clear map
        $this->initEmptyGameField();

        if (!$this->game->playerStart) {
            $this->playAI();
        }

    }

    /**
     * @param int $playerSymbol
     * @param array $map
     * @return int Moves count
     */
    protected function countMoves(int $playerSymbol, array $map): int
    {
        $count = 0;
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                if ($map[$col][$row] == $playerSymbol) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function initEmptyGameField(): array
    {
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                $this->game->field[$col][$row] = 0;
            }
        }
        return $this->game->field;
    }

    public function giveUp()
    {
        if (!isset($this->game->surrendered)) {
            $this->game->surrendered = 1;
            $this->game->playerAIScore++;
        }
    }

    /**
     * @return bool
     */
    public function checkDraw(): bool
    {
        return !$this->in_array_r(0, $this->game->field);
    }

    public function logout()
    {
        $this->game->remove();
    }

    public function createSPGame(int $playerSymbol, bool $playerStart, int $scorePlayer = 0, int $scoreAI = 0)
    {
        $this->game->playerSymbol = $playerSymbol;
        $this->game->playerAISymbol = $playerSymbol == 2 ? 1 : 2;
        $this->game->playerScore = $scorePlayer;
        $this->game->playerAIScore = $scoreAI;
        $this->game->playerStart = $playerStart;
        $this->initEmptyGameField();
        if (!$playerStart) {
            $this->playAI();
        }
    }
    
    /**
     * @return bool|array Player ID
     */
    public function gameFindWin() // SP
    {
        for ($col = 0; $col < self::GAMEFIELD_SIZE; $col++) {
            for ($row = 0; $row < self::GAMEFIELD_SIZE; $row++) {
                $field = $this->game->field;
                $current = $field[$col][$row];
                if ($current != 0) {

                    /*
                     * win: O O O O O
                     */

                    if ($current == @$field[$col][$row+1] AND
                        $current == @$field[$col][$row+2] AND
                        $current == @$field[$col][$row+3] AND
                        $current == @$field[$col][$row+4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col, $row+1],
                            [$col, $row+2],
                            [$col, $row+3],
                            [$col, $row+4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win: O
                     *      O
                     *      O
                     *      O
                     *      O
                     */

                    elseif ($current == @$field[$col-1][$row] AND
                        $current == @$field[$col-2][$row] AND
                        $current == @$field[$col-3][$row] AND
                        $current == @$field[$col-4][$row]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row],
                            [$col-2, $row],
                            [$col-3, $row],
                            [$col-4, $row],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win: O
                     *       O
                     *        O
                     *         O
                     *          O
                     */

                    elseif ($current == @$field[$col-1][$row+1] AND
                        $current == @$field[$col-2][$row+2] AND
                        $current == @$field[$col-3][$row+3] AND
                        $current == @$field[$col-4][$row+4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row+1],
                            [$col-2, $row+2],
                            [$col-3, $row+3],
                            [$col-4, $row+4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                    /*
                     * win:     O
                     *         O
                     *        O
                     *       O
                     *      O
                     */

                    elseif ($current == @$field[$col-1][$row-1] AND
                        $current == @$field[$col-2][$row-2] AND
                        $current == @$field[$col-3][$row-3] AND
                        $current == @$field[$col-4][$row-4]) {
                        $winningRow = [
                            [$col, $row],
                            [$col-1, $row-1],
                            [$col-2, $row-2],
                            [$col-3, $row-3],
                            [$col-4, $row-4],
                        ];
                        $winner = $current;
                        break;
                        break;
                    }

                }
            }
        }
        return isset($winner) && isset($winningRow) ? ["name" => $winner, "row" => $winningRow] : false;
    }
}