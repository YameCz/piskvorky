<?php

namespace App\Presenters;

use App\Model\MultiPlayerManager;
use App\Model\SinglePlayerManager;
use Nette;
use App\Model\BaseRepository;

/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{
    public $mpManager, $game, $spManager, $db;

    public function __construct(Nette\Database\Context $context, MultiPlayerManager $mpManager, SinglePlayerManager $spManager)
    {
        parent::__construct();
        $this->mpManager = $mpManager;
        $this->spManager = $spManager;
        $this->game = $this->mpManager->game;
        $this->db = $context;
    }
}
