<?php
/**
 * Created by PhpStorm.
 * User: PanYame
 * Date: 14.1.2018
 * Time: 15:23
 */

namespace App\Presenters;

use Nette;
use Nette\Application\UI;

class MultiPlayerPresenter extends BasePresenter
{
    public function renderDefault()
    {
        if ($this->mpManager->checkLogin()) {
            if ($this->mpManager->isMapOk()) {
                $this->template->player1 = $this->mpManager->getPlayer();

                if ($this->mpManager->isSecondPlayerConnected()) {
                    $this->template->nowPlaying = $this->mpManager->whoIsPlaying();
                    $this->template->player2 = $this->mpManager->getPlayer2();
                    $this->template->lastMove = json_decode($this->mpManager->getGame()->last_move);
                    $this->template->gamefield = json_decode($this->mpManager->getMap());
                    $this->template->canOfferDraw = $this->mpManager->canOfferDraw();

                    if ($this->mpManager->gameFindWin() != false) {
                        $this->template->winner = $this->mpManager->gameFindWin();
                        unset($this->template->nowPlaying);

                    } else if ($this->mpManager->checkGiveUp() != false) {
                        $this->template->giveUp = true;
                        $this->template->loser = $this->mpManager->checkGiveUp() == $this->game->playerId ? $this->mpManager->getPlayer()->name : $this->mpManager->getPlayer2()->name;
                        unset($this->template->nowPlaying);

                    } else if ($this->mpManager->checkDraw()) {
                        $this->template->draw = true;
                        $this->mpManager->addPointsToScoreDraw();
                        unset($this->template->nowPlaying);
                    }

                    else if ($this->mpManager->checkDrawOffer()) {
                        $this->template->drawOffer = true;
                        unset($this->template->nowPlaying);
                    }

                    if ($this->mpManager->checkBothNewGame()) {
                        $this->mpManager->newGame();
                        $this->redirect("MultiPlayer:");
                    } else {
                        $this->template->player1NextGame = $this->mpManager->getPlayer()->next_game == 1 ? true : false;
                        $this->template->player2NextGame = $this->mpManager->getPlayer2()->next_game == 1 ? true : false;
                    }

                } else {
                    $this->template->waiting = true;
                    $this->template->inviteCode = $this->game->hash;
                }
            } else {
                $this->mpManager->logout();
                $this->flashMessage("Opponent has left the game.", "info");
                $this->redirect("Homepage:");
            }
        } else {
            $this->flashMessage("You are not logged in!", "error");
            $this->redirect("Homepage:");
        }
    }

    public function createComponentStartupForm()
    {
        $form = new UI\Form();

        $form->addText("nickname", "Nickname")
            ->setDefaultValue("Player 1")
            ->setRequired("Choose some nickname!");

        $form->addRadioList("player", "Select", [
            "1" => "Play as O",
            "2" => "Play as X"
        ]);

        $form->addCheckbox("playFirst", "Player will start a game.");

        $form->addSubmit("submit", "Launch a game");

        $form->addProtection();

        $form->onSuccess[] = [$this, "handleStartupForm"];
        
        return $form;
    }

    public function handlePlay(int $col, int $row)
    {
        if ($this->isAjax()) {
            if ($this->mpManager->canMove($col, $row) !== false) {
                $this->mpManager->play($col, $row, $this->game->playerId);
                $this->redrawControl();
            }
        }
    }

    public function handleRefresh()
    {
        if ($this->isAjax()) {
            $this->redrawControl();
        }
    }

    public function handleNewGame()
    {
        if ($this->mpManager->gameFindWin() !== false || $this->mpManager->checkDraw() || $this->mpManager->checkGiveUp() !== false) {
            $this->mpManager->wantNextGame($this->game->playerId);
            if ($this->mpManager->checkBothNewGame()) {
                $this->mpManager->newGame();
            }
        }
        $this->redrawControl();
    }

    public function handleOfferDraw()
    {
        if (!$this->mpManager->gameFindWin() && !$this->mpManager->checkDraw()) {
            if ($this->mpManager->canOfferDraw()) {
                $this->mpManager->offerDraw($this->game->playerId);
                $this->redrawControl();
            }
        }
    }

    public function handleAcceptDraw()
    {
        if ($this->mpManager->checkDrawOffer()) {
            $this->mpManager->castDraw();
        }
        $this->redrawControl();
    }

    public function handleDeclineDraw()
    {
        if ($this->mpManager->checkDrawOffer()) {
            $this->mpManager->declineDrawOffer();
            $this->redrawControl();
        }
    }

    public function handleGiveUp()
    {
        if (!$this->mpManager->gameFindWin() && !$this->mpManager->checkDraw()) {
            $this->mpManager->giveUp();
            $this->redrawControl();
        }
    }

    public function actionLogout()
    {
        $this->mpManager->logout();
        $this->flashMessage("You have left the game.", "success");
        $this->redirect("Homepage:");
    }

    public function actionPlayerLeft()
    {
        $this->mpManager->logout();
        $this->flashMessage("Opponent has left the game.", "info");
        $this->redirect("Homepage:");
    }

    public function actionStartup()
    {
        if (isset($this->game->gameId)) {
            $this->redirect("MultiPlayer:");
        } elseif (isset($this->game->field)) {
            $this->redirect("SinglePlayer:");
        }
    }

    public function renderInvite($code)
    {
        if (!isset($code)) {
            $this->flashMessage("Wrong invite link!", "error");
            $this->redirect("Homepage:");
        } else {
            if (!$this->mpManager->validateCode($code)) {
                $this->flashMessage("Wrong invite link!", "error");
                $this->redirect("Homepage:");
            } else {
                $this->template->code = $code;
            }
        }
    }

    public function createComponentInviteForm()
    {
        $form = new UI\Form();

        $form->addText("username", "Choose your name and start playing:")
            ->setDefaultValue("Player 2");

        $form->addHidden("code");

        $form->addSubmit("submit", "Join the game!");

        $form->addProtection();

        $form->onSuccess[] = [$this, "processInviteForm"];

        return $form;
    }

    public function processInviteForm(UI\Form $form, $values)
    {
        if ($this->mpManager->joinGame($values->code, $values->username)) {
            $this->redirect("MultiPlayer:");
        } else {
            $this->flashMessage("Players are already connected!", "error");
            $this->redirect("Homepage:");
        }
    }

    public function createComponentJoinGame()
    {
        $form = new UI\Form();

        $form->addText("code", "Invite code:")
            ->setRequired("You must insert a code!");

        $form->addSubmit("submit", "Join the game!");

        $form->addProtection();

        $form->onSuccess[] = [$this, "processJoinGame"];

        return $form;
    }

    public function processJoinGame(UI\Form $form, $values)
    {
        $this->redirect("MultiPlayer:invite", ["code"=>$values->code]);
    }

    public function handleStartupForm(UI\Form $form, $values) 
    {
        $this->mpManager->createGame($values->player, $values->nickname, $values->playFirst);
        $this->redirect("MultiPlayer:default");
    }
}