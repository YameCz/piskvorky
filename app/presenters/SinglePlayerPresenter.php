<?php

namespace App\Presenters;

use Nette;
use Nette\Application\UI;

class SinglePlayerPresenter extends BasePresenter
{

    public function renderDefault()
    {
        if (isset($this->game->field)) {
            $this->template->gamefield = $this->game->field;
            $this->template->playerScore = $this->game->playerScore;
            $this->template->playerSymbol = $this->game->playerSymbol;
            $this->template->playerAIScore = $this->game->playerAIScore;
            $this->template->playerAISymbol = $this->game->playerAISymbol;
            $this->template->surrendered = $this->game->surrendered;

            if ($this->spManager->checkDraw()) {
                $this->template->draw = true;
            } else {
                if ($this->spManager->gameFindWin() != false) {
                    $this->template->winner = $this->spManager->gameFindWin();
                    $this->redrawControl("scoreTable");
                }
            }

        } else {
            $this->flashMessage("You are not loged in!", "error");
            $this->redirect("Homepage:");
        }

    }

    public function actionStartup()
    {
        if (isset($this->game->field)) {
            $this->redirect("SinglePlayer:");
        } elseif (isset($this->game->gameId)) {
            $this->redirect("MultiPlayer:");
        }
    }

    public function handlePlay(int $col, int $row)
    {
        if ($this->isAjax()) {
            if ($this->spManager->canPlay($col, $row, $this->game->playerSymbol)) {
                if ($this->spManager->play($col, $row, $this->game->playerSymbol)) {
                    if ($this->spManager->gameFindWin() != false) {
                        $this->game->playerScore++;
                        $this->template->winner = $this->spManager->gameFindWin();
                    } else if ($this->spManager->checkDraw()) {
                        $this->game->playerScore++;
                        $this->game->playerAIScore++;
                        $this->template->draw = true;
                    } else {
                        $this->spManager->playAI();
                        if ($this->spManager->gameFindWin() != false) {
                            $this->game->playerAIScore++;
                            $this->template->winner = $this->spManager->gameFindWin();
                        } else if ($this->spManager->checkDraw()) {
                            $this->game->playerScore++;
                            $this->game->playerAIScore++;
                            $this->template->draw = true;
                        }
                    }
                }
            }
            $this->redrawControl();
        }
    }

    public function handleGiveUp()
    {
        if ($this->isAjax()) {
            $this->spManager->giveUp();
            $this->redrawControl();
        }
    }

    public function handleNewGame()
    {
        if ($this->isAjax()) {
            $this->spManager->newGame();
            $this->redrawControl();
        }
    }

    public function actionLogout()
    {
        $this->spManager->logout();
        $this->flashMessage("You have left the game!", "success");
        $this->redirect("Homepage:");
    }

    public function createComponentStartupForm()
    {
        $form = new UI\Form();

        $form->addRadioList("player", "Select", [
            "1" => "Play as O",
            "2" => "Play as X"
        ]);

        $form->addCheckbox("playFirst", "Player will play first.");

        $form->addSubmit("submit", "Start a game");

        $form->onSuccess[] = [$this, "createSPGame"];

        return $form;
    }

    public function createSPGame(UI\Form $form, $values)
    {
        $this->spManager->createSPGame($values->player, $values->playFirst);
        $this->redirect("SinglePlayer:");
    }
}

