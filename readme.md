Install guide
===============

Requirements:
------------
- composer
- webserver
- linux machine (other OS should work too)
- php 7.1+
- php extensions: intl, libxml, PDO, pdo_sqlite, xml, json, sqlite3

Step by step:
---------------------------
1. clone this repository to desired location
2. go to project location and run **composer install**
3. go to app/config and create file **config.local.neon**
4. go to app/model and rename gameEMPTY.db3 to **game.db3**
5. directories **app/model**, **temp** and **log** must have chmod 775
6. **app/model/game.db3** must have chmod 664
7. enjoy
